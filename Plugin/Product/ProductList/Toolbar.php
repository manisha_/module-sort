<?php

namespace Shoptimize\Sort\Plugin\Product\ProductList;

use Magento\Backend\Block\Dashboard\Tab\Products\Viewed;

class Toolbar extends \Magento\Framework\View\Element\Template
{
    protected $_coreRegistry = null;
    protected $_collectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $collectionFactory,
        \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_coreRegistry = $registry;
        $this->productsFactory = $productsFactory;
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * return collection of bestsellers
     */
    public function getBestSellerData()
    {
        $collection = $this->_collectionFactory->create()->setModel(
            'Magento\Catalog\Model\Product'
        );
        return $collection;
    }       

    /**
     * returns most viewed products
     */
    public function getMostViewed()
    {
        $currentStoreId = $this->_storeManager->getStore()->getId();

        $collection = $this->_productsFactory->create()
        ->addAttributeToSelect(
            '*'
        )->addViewsCount()->setStoreId(
                $currentStoreId
        )->addStoreFilter(
                $currentStoreId
        );
        $items = $collection->getItems();
        return $items;
    }


    public function setCollection($collection) {
        $this->_collection = $collection;
        $this->_collection->setCurPage($this->getCurrentPage());
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }
        if ($this->getCurrentOrder()) {
            switch ($this->getCurrentOrder()) {
            case 'created_at':
                if ( $this->getCurrentDirection() == 'desc' ) {
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at DESC');
                } elseif ( $this->getCurrentDirection() == 'asc' ) {
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at ASC');
                }
                break;
            case 'views':
                $this->_collection = $this->getMostViewed();
                if ( $this->getCurrentDirection() == 'desc' ) {
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at DESC');
                } elseif ( $this->getCurrentDirection() == 'asc' ) {
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at ASC');
                }
                break;
            case 'bestsellers':
                $this->_collection = $this->getBestSellerData();
                if ( $this->getCurrentDirection() == 'desc' ) {
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at DESC');
                } elseif ( $this->getCurrentDirection() == 'asc' ) {
                    $this->_collection
                        ->getSelect()
                        ->order('e.created_at ASC');
                }
                break;
                
            default:
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
                break;
            }
        }
        return $this;
    }
}
