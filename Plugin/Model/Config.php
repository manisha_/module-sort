<?php

namespace Shoptimize\Sort\Plugin\Model;

class Config extends \Magento\Eav\Model\Config
{

    public function afterGetAttributeUsedForSortByArray(
        \Magento\Catalog\Model\Config $catalogConfig,
        $options
    ) {
        $options['created_at'] = __('New');
        $options['views'] = __('Most Viewed');
        $options['bestsellers'] = __('Bestsellers');
        return $options;
    }
}

?>